/**
 * ALUNO: AFFONSO BRIAN PERERIRA AZEVEDO
 * RA: 160017
 * */
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>

int main(int argc, char **argv)
{
	/**
	 * pid - guarda o id do processo
	 * status - serve para saber quando o processo filho terminou sua
	 * execução
	 * i - usada como variavel de controle dentro do laço de repetição
	 * v1 e v2 - variaveis que recebem valores aleatório para realização
	 * de operações matematicas
	 * */
	pid_t pid;
	int status,i;
	int v1 = rand() % 229;
	int v2 = rand() % 158;
	
	/**
	 * Cria um processo filho para soma
	 * */
	pid = fork();

	if(pid<0)
	{
		printf("O processo filho não pode ser criado \n");
 		perror( argv[0] );
		exit( errno );
	}
	
	else if(pid == 0)
	{
		printf("%d + %d = %d\n",v1,v2,v1+v2);
		exit(EXIT_SUCCESS);
	}
	
	pid = fork();
	
	/**
	 * Cria um processo filho para subtração
	 * */
	if(pid<0)
	{
		printf("O processo filho não pode ser criado \n");
 		perror( argv[0] );
		exit( errno );
	}
	
	else if(pid == 0)
	{
		printf("%d - %d = %d\n",v1,v2,v1-v2);
		exit(EXIT_SUCCESS);
	}
	
	pid = fork();
	/**
	 * Cria um processo filho para Multiplicação
	 * */
	if(pid<0)
	{
		printf("O processo filho não pode ser criado \n");
 		perror( argv[0] );
		exit( errno );
	}
	
	else if(pid == 0)
	{
		printf("%d * %d = %d\n",v1,v2,v1*v2);
		exit(EXIT_SUCCESS);
	}
	
	pid = fork();
	/**
	 * Cria um processo filho para Divisao
	 * */	
	if(pid<0)
	{
		printf("O processo filho não pode ser criado \n");
 		perror( argv[0] );
		exit( errno );
	}
	
	else if(pid == 0)
	{
		printf("%d / %d = %d\n",v1,v2,v1/v2);
		exit(EXIT_SUCCESS);
	}
	
	/**
	 * Espera os 4 processos filhos terminarem sua execução
	 * */
	for(i=0;i<4;i++)
	{
		wait(&status);
	}
	
	/**
	 * Finaliza o processo pai com sucesso
	 * */
	exit(EXIT_SUCCESS);
}
