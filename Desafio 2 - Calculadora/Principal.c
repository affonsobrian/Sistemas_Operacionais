#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>

int main(int argc, char **argv)
{
 	/**
	 * pid irá guardar o process id para saber quem é o processo pai e
	 * quem é o processo filho.
	 * status servirá para saber quando o processo filho terminar de exe
	 * cutar sua tarefa.
	 * fd servirá, para abrir o arquivo de comunicação entre os procs
	 * soma,sub,div e mult são vetores que guardam respectivamente
	 * valor1, valor2 e resultado de cada uma das operações
	 * aux serve para copiar os valores escritos no arquivo para a 
	 * memoria ram onde podemos converter a string novamente para int
	 * A variavel i serve como contador dentro dos laçoes de repetição
	 * */
	pid_t pid;
	int status,fd;
	int soma[3], sub[3], div[3], mult[3];
	char aux[10];
	int i;
	
	/**
	 * Cria o arquivo chamado pai onde ocorem as msgs
	 * */
	mknod("pai",S_IFIFO | 0666,0);
	
	
	/**
	 * Separa um processo pai e um processo filho
	 * */
	pid=fork();
	
	/**
	 * Se pid<0 exibe um erro
	 * */
	if(pid<0)
	{
		printf("O processo filho não pode ser criado \n");
 		perror( argv[0] );
		exit( errno );
	}
	
	/**
	 * O processo pai tenta abrir o arquivo chamado 'pai' e espera
	 * o processo filho retornar
	 * */
	else if(pid>0)
	{
		/**
		 * Abre o arquivo chamado pai, O_NDELAY Significa Read Only
		 * */
		fd=open("pai",  O_RDONLY | O_NDELAY);
		
		/**
		 * Se fd<0 houve erro na abertura do arquivo
		 * */
		if (fd < 0)
		{
			perror(argv[0]);
			exit (EXIT_FAILURE);
		}
		
		/**
		 * Espera o processo filho
		 * */
		wait(&status);
		printf("PAI: Processo filho de soma encerrado\n");
		
		/**
		 * Lê o conteudo escrito no arquivo 'pai' guarda na variavel aux
		 * de aux copia para cada posição do vetor soma
		 * */
		for(i=0;i<3;i++)
		{
			read(fd,aux,sizeof(aux));
			soma[i]=atoi(aux);
		}
		close(fd);
	}
	
	/**
	 * O processo filho manda executar o arquivo Soma
	 * */
	else
	{
		execl("Soma","Soma",NULL);
	}
	
	printf("\n\n");
	
	/**
	 * Separa um processo pai e um processo filho
	 * */
	pid=fork();
	
	/**
	 * Se pid<0 exibe um erro
	 * */
	if(pid<0)
	{
		printf( "O processo filho não pode ser criado \n");
 		perror( argv[0] );
		exit( errno );
	}
	/**
	 * O processo pai tenta abrir o arquivo chamado 'pai' e espera
	 * o processo filho retornar
	 * */
	else if(pid>0)
	{
		/**
		 * Abre o arquivo chamado pai, O_NDELAY Significa Read Only
		 * */
		fd=open("pai",  O_RDONLY | O_NDELAY);
		
		/**
		 * Se fd<0 houve erro na abertura do arquivo
		 * */
		if (fd < 0)
		{
			perror(argv[0]);
			exit (EXIT_FAILURE);
		}
		
		/**
		 * Espera o processo filho
		 * */
		wait(&status);
 
		printf("PAI: Processo filho de subtração encerrado\n");
		/**
		 * Lê o conteudo escrito no arquivo 'pai' guarda na variavel aux
		 * de aux copia para cada posição do vetor soma
		 * */

		for(i=0;i<3;i++)
		{
			read(fd,aux,sizeof(aux));
			sub[i]=atoi(aux);
		}
		close(fd);
	}
	/**
	 * O processo filho abre o arquivo Subtracao
	 * */
	else
	{
		execl("Subtracao","Subtracao",NULL);
	}
	//sleep(3);
	
	printf("\n\n");
	
	/**
	 * Separa um processo pai e um processo filho
	 * */
	pid=fork();
	
	/**
	 * Se pid<0 exibe um erro
	 * */
	if(pid<0)
	{
		printf("O processo filho não pode ser criado \n");
 		perror(argv[0]);
		exit(errno );
	}
	
	/**
	 * O processo pai tenta abrir o arquivo chamado 'pai' e espera
	 * o processo filho retornar
	 * */
	else if(pid>0)
	{
		/**
		 * Abre o arquivo chamado pai, O_NDELAY Significa Read Only
		 * */
		fd=open("pai",  O_RDONLY | O_NDELAY);
		
		/**
		 * Se fd<0 houve erro na abertura do arquivo
		 * */
		if (fd < 0)
		{
			perror(argv[0]);
			exit (EXIT_FAILURE);
		}
		
		/**
		 * Espera o processo filho
		 * */
		wait(&status);
 
		printf("PAI: Processo filho de multiplicação encerrado\n");

		/**
		 * Lê o conteudo escrito no arquivo 'pai' guarda na variavel aux
		 * de aux copia para cada posição do vetor soma
		 * */
		for(i=0;i<3;i++)
		{
			read(fd,aux,sizeof(aux));
			mult[i]=atoi(aux);
		}
		close(fd);
	}
	/**
	 * O processo filho executa o arquivo Mutiplicacao
	 * */
	else
	{
		execl("Multiplicacao","Multiplicacao",NULL);
	}
	//sleep(3);
	
	printf("\n\n");
	
	/**
	 * Separa um processo pai e um processo filho
	 * */
	pid=fork();
	
	/**
	 * Se pid<0 exibe um erro
	 * */
	if(pid<0)
	{
		printf( "O processo filho não pode ser criado \n");
 		perror( argv[0] );
		exit( errno );
	}
	/**
	 * O processo pai tenta abrir o arquivo chamado 'pai' e espera
	 * o processo filho retornar
	 * */
	else if(pid>0)
	{
		/**
		 * Abre o arquivo chamado pai, O_NDELAY Significa Read Only
		 * */
		fd=open("pai", O_RDONLY | O_NDELAY);
		
		/**
		 * Se fd<0 houve erro na abertura do arquivo
		 * */
		if (fd < 0)
		{
			perror(argv[0]);
			exit (EXIT_FAILURE);
		}
		
		/**
		 * Espera o processo filho
		 * */
		wait(&status);
 
		printf("PAI: Processo filho de divisão encerrado\n");
		
		/**
		 * Lê o conteudo escrito no arquivo 'pai' guarda na variavel aux
		 * de aux copia para cada posição do vetor soma
		 * */
		for(i=0;i<3;i++)
		{
			read(fd,aux,sizeof(aux));
			div[i]=atoi(aux);
		}
		close(fd);
	}
	/**
	 * O processo filho executa o arquivo Divisao
	 * */
	else
	{
		execl("Divisao","Divisao",NULL);
	}
	
	printf("\n");
	printf("SOMA: %i + %i = %i\n",soma[0],soma[1],soma[2]);
	printf("SUBT: %d - %d = %d\n",sub[0],sub[1],sub[2]);
	printf("MULT: %d * %d = %d\n",mult[0],mult[1],mult[2]);
	printf("DIVI: %d / %d = %d\n",div[0],div[1],div[2]);
	exit(EXIT_SUCCESS);
}
