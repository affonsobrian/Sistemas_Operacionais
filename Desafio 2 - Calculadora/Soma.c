#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>

int main(int argc, char **argv)
{
	/**
	 * As variaveis v1, v2, são para realizar as contas de soma,seus va-
	 * lores são obtidos de forma randomica.
	 * A variavel fd serve para abrir o arquivo que servirá para a comu-
	 * nicação entre os processos.
	 * As variaveis res, a e b servem para receber o valores gravados no
	 * arquivo na forma de string (vetor de caracteres)
	 * */
	int v1,v2,fd;
	char res[10], a[10], b[10];
	
	/**
	 * Inicializa v1 e v2 de forma 'randomica'
	 * */
	v1=rand()%12;
	v2=rand()%1;
	
	/**
	 * Transforma os valores inteiros em strings para enviar para poder
	 * enviar para o arquivo de comunicação.
	 * */
	sprintf(a, "%d", v1);
	sprintf(b, "%d", v2);
	sprintf(res, "%d", v1+v2);
	
	printf("FILHO: %i + %i\n",v1,v2);
	
	/**
	 * Abre o arquivo, se fd<0 significa que houve algum problema para 
	 * abrir o arquivo.
	 * */
	fd=open("pai", O_WRONLY | O_NDELAY);
	if(fd<0)
	{
		perror(argv[0]);
		exit (EXIT_FAILURE);
	}
	
	/**
	 * Escreve os valores a, b e res no arquivo, em seguida fecha-o.
	 * */
	write(fd,a,sizeof(a));
	write(fd,b,sizeof(b));
	write(fd,res,sizeof(res));
	close(fd);
	
	/**
	 * Finaliza o processo filho de Mutiplicacao
	 * */
	printf("FILHO: Finalizando processo\n");
	exit(EXIT_SUCCESS);
}

